var toDoApp = {
    id: 0,


    items: [],

    generateItemHTML: function( item ) {
        var date = new Date(item.date);
        var html = "";

        var now = Date.now();

        date.setTime( date.getTime() + date.getTimezoneOffset()*60*1000 );

        html += '<p data-id="'+ item.id +'" class="item">';
        html += '<span class="item-text">' + item.text + '</span>';

        if(item.date) {
            html += '<span class="item-date">' + date.toLocaleDateString();

            if(now > item.date) {
                html += '!!';
            }

            html += '</span>';

        }

        html += '<i class="remove fa fa-times pull-right"></i>';

        if(item.completed) {
            html += '<i class="checkbox-empty fa fa-check-square pull-right"></i>'
        } else {
            html += '<i class="checkbox-empty fa fa-square-o pull-right"></i>'
        }

        html += '</p>';

        var ele = $(html);

        if(item.completed) {
            ele.addClass('completed');
        }

        return ele;
    },

    showError: function() {
        $('#error').show();
    },

    hideError: function() {
        $('#error').hide();
    },

    addItem: function() {
        toDoApp.id++;

        var toAddItem = $('input[name=checkListItem]').val();
        var toAddDate = $('input[name=date]');
        var dateEle = toAddDate.get(0);
        var dateObj = dateEle.valueAsDate;

        if(toAddItem.length === 0) {
            toDoApp.showError();
            return;
        }

        //Add to list
        toDoApp.items.push( {
            id: toDoApp.id,
            text: toAddItem,
            date: dateObj
        } );

        //Save the list
        toDoApp.saveList();

        //Render the list
        toDoApp.render();
    },

    render: function() {
        var list = $('#list');
        list.empty();

        for(var i = 0; i < toDoApp.items.length; i++) {
            var next = toDoApp.items[i];

            var html = toDoApp.generateItemHTML(next);
            list.append(html);
        }

        toDoApp.hideError();

        $('input[name=checkListItem]').val('');
        $('input[name=date]').val('');
    },

    deleteItem: function() {
        var id = $(this).parent().data('id');
        for(var i = 0; i < toDoApp.items.length; i++) {
            var next = toDoApp.items[i];

            if(id === next.id) {
                toDoApp.items.splice( i, 1 );
                break;
            }
        }

        toDoApp.saveList();
        toDoApp.render();

        return false;
    },

    getItemById: function( id ) {
        for(var i = 0; i < toDoApp.items.length; i++) {
            var next = toDoApp.items[i];

            if(id === next.id) {
                return next;
            }
        }

        return null;
    },

    markCompleted: function() {
        var id = $(this).parent().data('id');
        var item = toDoApp.getItemById( id );
        item.completed = !item.completed;
        toDoApp.saveList();
        toDoApp.render();
    },


    saveList: function() {
        var jsonString = JSON.stringify( toDoApp.items );
        localStorage.setItem('list', jsonString );
        localStorage.setItem( 'itemId', toDoApp.id );
    },

    loadList: function() {
        //if we have something in local storage, place it on page
        var list = localStorage.getItem('list');

        if(list !== null) {
            toDoApp.items = JSON.parse( list );
            toDoApp.id = localStorage.getItem( 'itemId' );
        };
    },

    init: function() {
        $('.button').click( toDoApp.addItem );
        $(document).on('click', '.item .remove', toDoApp.deleteItem);
        $(document).on('click', '.checkbox-empty', toDoApp.markCompleted );

        toDoApp.loadList();
        toDoApp.render();
    }
};


$( toDoApp.init );